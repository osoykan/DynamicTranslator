﻿namespace DynamicTranslator.Core.Dependency
{
    public interface IDisposableDependencyObjectWrapper : IDisposableDependencyObjectWrapper<object>
    {
    }
}